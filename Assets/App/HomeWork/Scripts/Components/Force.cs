using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.HomeWork.Component
{
    [NodeGraphGroupName("Homework")]
    //[DisallowMultipleComponent]
    public class Force : CoreComponent
    {
        [SerializeField] private Vector3 _force;

        public Vector3 ForceValue
        {
            get => _force;
            set
            {
                if (value.y < 0)
                    return;
                _force = value;
            }
        }
    }
}