using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.HomeWork.Component
{
    [NodeGraphGroupName("Homework")]
    [DisallowMultipleComponent]
    public class Chair : CoreComponent
    {
        private Transform transform;
        public Vector3 position;

        private void Awake()
        {
            transform = GetComponent<Transform>();
            position = transform.position;
        }
    }
}