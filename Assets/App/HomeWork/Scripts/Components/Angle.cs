using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.HomeWork.Component
{
    [NodeGraphGroupName("Homework")]
    [DisallowMultipleComponent]
    public class Angle : CoreComponent
    {
        public float AngleValue { get; set; }
    }
}