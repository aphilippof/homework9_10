using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;
using UnityEngine.EventSystems;

namespace App.HomeWork.Component
{
    [NodeGraphGroupName("Homework")]
    [DisallowMultipleComponent]
    public class ClickHandler : CoreComponent, IPointerDownHandler
    {
        public OutputSignal OnClick;

        public void OnPointerDown(PointerEventData eventData)
        {
            OnClick.Invoke();
        }
    }
}