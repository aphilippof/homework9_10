using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.HomeWork.Component
{
    [NodeGraphGroupName("Homework")]
    [DisallowMultipleComponent]
    public class UpdateHandler : CoreComponent
    {
        public OutputSignal OnUpdate;

        private void Update()
        {
            OnUpdate.Invoke();
        }
    }
}