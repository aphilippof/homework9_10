using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.HomeWork.Component
{
    [NodeGraphGroupName("Homework")]
    [DisallowMultipleComponent]
    public class CircleCenter : CoreComponent
    {
        private Vector3 center;

        public Vector3 Center { get; set; }
    }
}