using App.HomeWork.Component;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.HomeWork.Action
{
    [NodeGraphGroupName("Homework")]
    public class AddForce : CoreAction
    {
        public InputComponent<Force> force;

        [InjectComponent] public Rigidbody rigidbody;
        [InjectComponent] public Transform transform;

        protected override bool Action()
        {
            rigidbody.AddForce(force.Component.ForceValue);


            return true;
        }
    }
}