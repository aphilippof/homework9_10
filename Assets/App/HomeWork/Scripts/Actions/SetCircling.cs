using App.HomeWork.Component;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.HomeWork.Action
{
    [NodeGraphGroupName("Homework")]
    public class SetCircling : CoreAction
    {
        public InputComponent<IsCircling> circling;
        public InputComponent<CircleCenter> circleCenter;
        
        [InjectComponent] public Transform transform;

        protected override bool Action()
        {
            // ���� ��������� � ����� �������� �� �����:
            // ��������� ����� ����� �������� ��� ������� ��������� ����
            if (!circling.Component.IsCirclingValue)
            {
                circleCenter.Component.Center = new Vector3(transform.position.x, transform.position.y, transform.position.z);

            }

            return true;
        }
    }
}