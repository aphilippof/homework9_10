using App.HomeWork.Component;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.HomeWork.Action
{
    [NodeGraphGroupName("Homework")]

    // ����: ����� �� �����, �� ����� ����������, ������� ��� ������, ������� �������
    
    // "������� �� �����, ������ ���� ����� �� ����, ����� ������ ��������
    // ������ ����"
    public class ClickAction : CoreAction
    {
        [InjectComponent] public Transform transform;
        [InjectComponent] public MeshFilter meshFilter;

        public InputComponent<IsCircling> isCircling;
        public InputComponent<Angle> angle;

        protected override bool Action()
        {
            // ���� �� � ������ ����� �� � �������� �� �����, �� ������ ���
            if (!isCircling.Component.IsCirclingValue)
            {
                StartMovingAround();
            }
            else
            {
                CompleteMovingAround();
            }

            return true;
        }

        private void StartMovingAround()
        {
            angle.Component.AngleValue = 20;
            // ��� ����� ������� ���� ����� � �������
            transform.Translate(new Vector3(-5 * meshFilter.mesh.bounds.size.x, 0, 0));

            // ������� �������� �� �����
            isCircling.Component.IsCirclingValue = true;
        }

        // ������������ ������� �� ����. ���� ������ ������ ����, ���� �� ����� � ����� ����� �����
        private void CompleteMovingAround()
        {
            if (isCircling.Component.IsCirclingValue)
            {
                // ��� ����� ������� ���� ����� � �����
                transform.Translate(new Vector3(5 * meshFilter.mesh.bounds.size.x, 0, 0));

                // ������� �������� �� �����
                isCircling.Component.IsCirclingValue = false;
            }
        }
    }
}