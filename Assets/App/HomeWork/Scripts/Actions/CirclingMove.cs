using App.HomeWork.Component;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.HomeWork.Action
{
    [NodeGraphGroupName("Homework")]

    // �������� �� �����
    public class CirclingMove : CoreAction
    {
        public InputComponent<IsCircling> isCircling;
        public InputComponent<CircleCenter> circleCenter;
        public InputComponent<Angle> angle;
        public InputComponent<Chair> chair;

        [InjectComponent] public Transform transform;

        protected override bool Action()
        {
            if (isCircling.Component.IsCirclingValue)
            {
                transform.transform.RotateAround(chair.Component.position, Vector3.up, angle.Component.AngleValue * Time.deltaTime);
            }

            return true;
        }
    }
}